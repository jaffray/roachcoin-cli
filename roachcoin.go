package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

var registerArgs = flag.NewFlagSet("register", flag.ExitOnError)
var registerName = registerArgs.String("handle", "", "the name to use")

var transferArgs = flag.NewFlagSet("transfer", flag.ExitOnError)
var transferTo = transferArgs.String("to", "", "the user to transfer to")
var transferAmt = transferArgs.String("amount", "", "the amount to transfer")
var transferMsg = transferArgs.String("message", "", "message to include with the transfer")

var addArgs = flag.NewFlagSet("add", flag.ExitOnError)
var addAmt = addArgs.String("amount", "", "the amount to add")

// const host = "localhost"

const host = "104.236.27.10"

func post(path string, payload map[string]interface{}) (map[string]interface{}, error) {
	return sendMsg("POST", path, payload)
}

func get(path string, payload map[string]interface{}) (map[string]interface{}, error) {
	return sendMsg("GET", path, payload)
}

func sendMsg(method string, path string, payload map[string]interface{}) (map[string]interface{}, error) {
	j, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, "http://"+host+":3000"+path, bytes.NewBuffer(j))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var result interface{}
	if err := json.Unmarshal(body, &result); err != nil {
		return nil, err
	}
	r := result.(map[string]interface{})
	if r["result"].(string) == "error" {
		return nil, fmt.Errorf("%s", r["msg"])
	}
	return result.(map[string]interface{}), nil
}

var fname = os.Getenv("HOME") + "/.roachcoin"

// set stores a value persistently.
func set(name string, value interface{}) error {
	d, err := ioutil.ReadFile(fname)
	if err != nil {
		// couldn't load the file
		d = []byte("{}")
	}

	var result map[string]interface{}
	json.Unmarshal([]byte(d), &result)
	result[name] = value

	enc, err := json.Marshal(result)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(fname, enc, 0644)
}

// get stores a value persistently.
func getProp(name string) (interface{}, bool) {
	d, err := ioutil.ReadFile(fname)
	if err != nil {
		// couldn't load the file
		d = []byte("{}")
	}

	var result map[string]interface{}
	json.Unmarshal([]byte(d), &result)
	res, ok := result[name]
	return res, ok
}

func run() error {
	if len(os.Args) < 2 {
		fmt.Println("available commands: register, balance, news, transfer, users")
		return nil
	}
	switch os.Args[1] {
	case "users":
		msg := map[string]interface{}{}
		res, err := get("/users", msg)
		if err != nil {
			return err
		}
		fmt.Println("roachcoin users:")
		for _, u := range res["users"].([]interface{}) {
			fmt.Println("  " + u.(map[string]interface{})["name"].(string))
		}
	case "register":
		if len(os.Args) <= 2 {
			fmt.Println("register usage: roachcoin register -handle <desired username>")
			return nil
		}
		registerArgs.Parse(os.Args[2:])
		msg := map[string]interface{}{
			"name": *registerName,
		}
		res, err := post("/user/register", msg)
		if err != nil {
			return err
		}
		set("id", res["id"])
		fmt.Println("you have registered as " + *registerName + " and your login info has been stored in ~/.roachcoin.")
	case "balance":
		id, ok := getProp("id")
		if !ok {
			return fmt.Errorf("need to register an account")
		}

		msg := map[string]interface{}{}

		res, err := get("/user/balance/"+id.(string), msg)
		if err != nil {
			return err
		}

		fmt.Println(res["balance"])
	case "news":
		transferArgs.Parse(os.Args[2:])

		id, ok := getProp("id")
		if !ok {
			return fmt.Errorf("need to register an account")
		}

		msg := map[string]interface{}{}

		res, err := get("/user/transfers/"+id.(string), msg)
		if err != nil {
			return err
		}

		for i, t := range res["transfers"].([]interface{}) {
			if i != 0 {
				fmt.Println(" ---- ")
			}
			tm := t.(map[string]interface{})
			fmt.Println("from: ", tm["fromname"])
			fmt.Println("amount: ", tm["amount"])
			fmt.Println("message: ", tm["message"])
		}
	case "transfer":
		if len(os.Args) <= 2 {
			fmt.Println("transfer usage: roachcoin transfer -amount <amt> -to <user>")
			return nil
		}
		transferArgs.Parse(os.Args[2:])

		id, ok := getProp("id")
		if !ok {
			return fmt.Errorf("need to register an account")
		}

		amt, err := strconv.Atoi(*transferAmt)
		if err != nil {
			return err
		}

		msg := map[string]interface{}{
			"id":      id,
			"to":      *transferTo,
			"amount":  amt,
			"message": *transferMsg,
		}

		_, err = post("/transfer", msg)
		if err != nil {
			return err
		}

		fmt.Println("transfer completed")
	case "add":
		addArgs.Parse(os.Args[2:])

		id, ok := getProp("id")
		if !ok {
			return fmt.Errorf("need to register an account")
		}

		amt, err := strconv.Atoi(*addAmt)
		if err != nil {
			return err
		}

		msg := map[string]interface{}{
			"id":     id,
			"amount": amt,
		}

		res, err := post("/user/add", msg)
		if err != nil {
			return err
		}

		fmt.Println(res)
	}
	return nil
}

func main() {
	flag.Parse()

	if err := run(); err != nil {
		fmt.Println(err)
	}
}
